
#include <Wire.h>
#include <LiquidCrystal.h>
const int MPU = 0x68; 
float AccX, AccY, AccZ;
float GyroX, GyroY, GyroZ;
float accAngleX, accAngleY, gyroAngleX, gyroAngleY, gyroAngleZ;
float roll, pitch, yaw;
float AccErrorX, AccErrorY, GyroErrorX, GyroErrorY, GyroErrorZ;
float elapsedTime, currentTime, previousTime;
int c = 0;
bool endwave=true;
float calibrow;
float calibrpi;
float calibry;
bool disparar;
int ammoindex;
String incoming;
int municion01=24;
int municion02=24;
int municion03=24;
int municion04=24;
int currentammo=1;
int reloadtimes1=2;
int reloadtimes2=2;
int reloadtimes3=2;
int reloadtimes4=2;
LiquidCrystal lcd(5, 4, 11, 10, 9, 8);


void setup() {
  Serial.begin(9600);
  Wire.begin();                
  Wire.beginTransmission(MPU); 
  Wire.write(0x6B);            
  Wire.write(0x00);            
  Wire.endTransmission(true);  
  pinMode(8,OUTPUT);
  pinMode(9,OUTPUT);
  calculate_IMU_error();
  delay(20);
  digitalWrite(8,LOW);
  digitalWrite(9,LOW);
  lcd.begin(16,2);
  lcd.home();
  pinMode(7, INPUT);

}
void loop() {
  
for (int i = 0; i < 24; i++)
  {
    lcd.setCursor(i,0);
    lcd.print(" ");
    lcd.setCursor(i,1);
    lcd.print(" ");
  }
  //Count();
  Bullets();
  printBullets();
  Wire.beginTransmission(MPU);
  Wire.write(0x3B); 
  Wire.endTransmission(false);
  Wire.requestFrom(MPU, 6, true);
 Recalibrar();

  AccX = (Wire.read() << 8 | Wire.read()) / 16384.0; 
  AccY = (Wire.read() << 8 | Wire.read()) / 16384.0; 
  AccZ = (Wire.read() << 8 | Wire.read()) / 16384.0; 

  accAngleX = (atan(AccY / sqrt(pow(AccX, 2) + pow(AccZ, 2))) * 180 / PI) + 6.63; 
  accAngleY = (atan(-1 * AccX / sqrt(pow(AccY, 2) + pow(AccZ, 2))) * 180 / PI) + 4.03; 
  
  previousTime = currentTime;        
  currentTime = millis();            
  elapsedTime = (currentTime - previousTime) / 1000; 
  Wire.beginTransmission(MPU);
  Wire.write(0x43); 
  Wire.endTransmission(false);
  Wire.requestFrom(MPU, 6, true); 
  GyroX = (Wire.read() << 8 | Wire.read()) / 131.0; 
  GyroY = (Wire.read() << 8 | Wire.read()) / 131.0;
  GyroZ = (Wire.read() << 8 | Wire.read()) / 131.0;
  
  GyroX = GyroX + 1.13; 
  GyroY = GyroY + 0.19; 
  GyroZ = GyroZ + 1.66; 
  
  gyroAngleX = gyroAngleX + GyroX * elapsedTime; 
  gyroAngleY = gyroAngleY + GyroY * elapsedTime;
  yaw =  yaw + GyroZ * elapsedTime;
  
  roll = 0.96 * gyroAngleX + 0.04 * accAngleX;
  pitch = 0.96 * gyroAngleY + 0.04 * accAngleY;

  calibrow+=0.10;
  calibrpi=0.053;
  roll+=calibrow;
  pitch-=calibrpi;
  calibry=0.00010;
  yaw-=0.07-calibry;
  
  Serial.print(int(roll));
  Serial.print("/");
  Serial.print(int(pitch));
  Serial.print("/");
  Serial.println(int(yaw));
  
}
void calculate_IMU_error() {
  
  while (c < 200) {
    Wire.beginTransmission(MPU);
    Wire.write(0x3B);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU, 6, true);
    AccX = (Wire.read() << 8 | Wire.read()) / 16384.0 ;
    AccY = (Wire.read() << 8 | Wire.read()) / 16384.0 ;
    AccZ = (Wire.read() << 8 | Wire.read()) / 16384.0 ;
    
    AccErrorX = AccErrorX + ((atan((AccY) / sqrt(pow((AccX), 2) + pow((AccZ), 2))) * 180 / PI));
    AccErrorY = AccErrorY + ((atan(-1 * (AccX) / sqrt(pow((AccY), 2) + pow((AccZ), 2))) * 180 / PI));
    c++;
  }
  
  AccErrorX = AccErrorX / 200;
  AccErrorY = AccErrorY / 200;
  c = 0;
  
  while (c < 200) {
    Wire.beginTransmission(MPU);
    Wire.write(0x43);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU, 6, true);
    GyroX = Wire.read() << 8 | Wire.read();
    GyroY = Wire.read() << 8 | Wire.read();
    GyroZ = Wire.read() << 8 | Wire.read();
    
    GyroErrorX = GyroErrorX + (GyroX / 131.0);
    GyroErrorY = GyroErrorY + (GyroY / 131.0);
    GyroErrorZ = GyroErrorZ + (GyroZ / 131.0);
    c++;
  }
  
  GyroErrorX = GyroErrorX / 200;
  GyroErrorY = GyroErrorY / 200;
  GyroErrorZ = GyroErrorZ / 200;
  
}

void Count(){

  if(endwave)
  {
    for(int i=0;i<5;i++)
    {
        digitalWrite(9,LOW);
        delay(2000);  
        digitalWrite(8,HIGH);
        delay(2000);
        digitalWrite(8,LOW);
    }
    endwave=false;
  }
 if(endwave==false)
 {
  digitalWrite(9,HIGH);
 }
}
void Bullets()
{
  if(Serial.available()>0)
  {
    incoming = Serial.read();
    if(incoming=="115")
    {
      disparar=true;
    }
      
      
    if(incoming=="49")
      ammoindex=1;
    else if (incoming=="50")
      ammoindex=2;
    else if (incoming=="51")
      ammoindex=3;
    else if (incoming=="52")
      ammoindex=4;   
    else if(incoming=="114")
    {
       if(currentammo==1 && reloadtimes1>0)
       {
        municion01+=24;
        reloadtimes1--;          
       }
       else if(currentammo==2 && reloadtimes2>0)
       {
        municion02+=24;
        reloadtimes2--; 
       }
       else if(currentammo==3 && reloadtimes3>0)
       {
        municion03+=24;
        reloadtimes3--; 
       }
       else if(currentammo==4 && reloadtimes4>0)
       {
        municion04+=24;
        reloadtimes4--; 
       }              
    }    


  }
   
}
void printBullets()
{
  
if(ammoindex==1)
  {
    if(disparar)
    {
        municion01--;
        disparar=false;
        currentammo=1;
    }
    for(int i=0; i<municion01;i++)
    {
      lcd.setCursor(i,1);
      lcd.print("X"); 
    }       
  }
              
  
else if(ammoindex==1)
  {
    if(disparar)
    {
        municion02--;
        disparar=false;
        currentammo=2;
    }
    for(int i=0; i<municion02;i++)
    {
      lcd.setCursor(i,1);
      lcd.print("Z"); 
    }       
  }
  
  else if(ammoindex==2)
  {
    if(disparar)
    {
        municion03--;
        disparar=false;
        currentammo=3;
    }
    for(int i=0; i<municion03;i++)
    {
      lcd.setCursor(i,1);
      lcd.print("R"); 
    }       
  }
  
  else if(ammoindex==3)
  {
    if(disparar)
    {
        municion04--;
        disparar=false;
        currentammo=4;
    }
    for(int i=0; i<municion04;i++)
    {
      lcd.setCursor(i,1);
      lcd.print("D"); 
    }       
  }

}
void Recalibrar()
{
  if(digitalRead(7)==LOW)
  {
    if(digitalRead(7)==HIGH)
    {
      roll=0;
      pitch=0;
      yaw=0;
    }
  }
}
void GameOver()
{
  if(municion01==0 && municion02==0 && municion03==0&& municion04==0)
  {
    lcd.setCursor(0,0);
    lcd.print("GAME");
    lcd.setCursor(1,0);
    lcd.print("OVER");
  }

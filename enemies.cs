﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemies : MonoBehaviour
{

     public List<Transform> waypoints = new List<Transform>();
     private int index=1;
     public float speed =4;
     public float rota =6;
     private Animator anim;
     private bool dead;
     public float maxlife=100;
     public float currentlife=0;
     public Image relleno;
     private Transform rotcanvas;
     private Quaternion initliverot;



     private void Awake()
     {
         anim= GetComponent<Animator>();
         anim.SetBool("move", true);
         rotcanvas=relleno.transform.parent.parent;
         initliverot = rotcanvas.rotation;
        GetWaypoint();
     }

    // Start is called before the first frame update
    void Start()
    {
        currentlife=maxlife;
        
    }

    // Update is called once per frame
    void Update()
    {
        rotcanvas.transform.rotation= initliverot;
        Movement();
        changeview();
    }
    private void Movement()
    {
        if(dead)
        {
            return;
        }
        transform.position = Vector3.MoveTowards(transform.position,waypoints[index].position,speed*Time.deltaTime);
        var distance = Vector3.Distance(transform.position, waypoints[index].position);
        if (distance<=1.0f)
        {
            if(index>=waypoints.Count-1)
            {
                return;
            }
            index++;
        } 
    }
    private void changeview()
    {
        if(dead)
        {
            return;
        }
        //Transform.LookAt(waypoints[index]);
        var dir= waypoints[index].position-transform.position;
        var rot= Quaternion.LookRotation(dir);
        transform.rotation= Quaternion.Slerp(transform.rotation, rot, rota*Time.deltaTime);

    }

    public void TakeDamage(float dmg)
    {
        var newLife= currentlife-dmg;
        if(dead)
        {
            return;
        }
        if(newLife<=0)
        {
            Dead();
        }
        currentlife= newLife;
        var fillvalue =currentlife*1/100;
        relleno.fillAmount=fillvalue;
        currentlife= newLife;
        StartCoroutine(AnimDamage());

    }
    private IEnumerator AnimDamage()
    {
        anim.SetBool("takedamage", true);
        yield return new WaitForSeconds(0.1f);
        anim.SetBool("takedamage", true);
    }
    private void Dead()
    {
        dead= true;
        anim.SetBool("takedamage", false);
        anim.SetBool("die", true);
        currentlife=0;
        relleno.fillAmount=0;
        StartCoroutine(AnimDead());
    }
    private IEnumerator AnimDead()
    {
        yield return new WaitForSeconds(1f);
        var finalY= transform.position.y-5;
        Vector3 target = new Vector3(transform.position.x, finalY, transform.position.z);
        while(transform.position.y != finalY)
        {
            transform.position= Vector3.MoveTowards(transform.position, target, 1.5f * Time.deltaTime);
            yield return null;
        }
        Destroy(this);
    }
    private void GetWaypoint()
    {
        waypoints.Clear();
        var rootWaypoint = GameObject.Find("waypoints").transform;
        for(int i =0; i <rootWaypoint.childCount; i++)
        {
            waypoints.Add(rootWaypoint.GetChild(i));
        }
    }
}
 
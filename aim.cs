using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aim : MonoBehaviour
{
 public GameObject bullet;
 public Transform spawnPoint;

 public float force=1000;
 public float rate=0.5f;
 public float maxammo=24;
 public float currentammo01=0;
 public float currentammo02=0;
 public float currentammo03=0;
 public float currentammo04=0;
 public int reloadtimes1=2;
 public int reloadtimes2=2;
 public int reloadtimes3=2;
 public int reloadtimes4=2;
 public int ammoindex=1;
 public bool shoot=false;



 private float ratetime=0;
    void Start()
    {
        currentammo01=maxammo;
        currentammo02=maxammo;
        currentammo03=maxammo;
        currentammo04=maxammo;

    }


    // Update is called once per frame
    void Update()
    {

        SelectAmmmo();
        TypeShoot();
        if(Input.GetButtonDown("Fire1"))
        {
            if(Time.time>ratetime)
            {
                shoot=true;
                GameObject newBullet;
                newBullet=Instantiate(bullet, spawnPoint.position, spawnPoint.rotation);

                newBullet.GetComponent<Rigidbody>().AddForce(spawnPoint.forward*force); 

                ratetime= Time.time + rate;

                Destroy(newBullet,2);
            } 

        }        

        
        
    }

    void SelectAmmmo()
    {
    if(Input.GetKeyDown(KeyCode.Alpha1))
        ammoindex=1;
    if(Input.GetKeyDown(KeyCode.Alpha2))
        ammoindex=2;
    if(Input.GetKeyDown(KeyCode.Alpha3))
        ammoindex=3;
    if(Input.GetKeyDown(KeyCode.Alpha4))
        ammoindex=4;
     if(Input.GetKeyDown(KeyCode.R))
    {
        if(ammoindex==1 && reloadtimes1>0 && currentammo01<=0)
        {
            currentammo01=24;
            reloadtimes1--;
        }
        if(ammoindex==2 && reloadtimes2>0&& currentammo02<=0)
        {
            currentammo02=24;
            reloadtimes2--;
        }
        if(ammoindex==3 && reloadtimes3>0&& currentammo03<=0)
        {
            currentammo03=24;
            reloadtimes3--;
        }
        if(ammoindex==4 && reloadtimes4>0&& currentammo04<=0)
        {
            currentammo04=24;
            reloadtimes4--;
        }                
    }           
        
    }
public void TypeShoot()
{
    if (ammoindex==1)
    {
        if(shoot==true)
        {
            currentammo01--;
            shoot=false;
            ammoindex=1;
            Debug.Log("Tipo 1");
        }
    }
    if (ammoindex==2)
    {
        if(shoot==true)
        {
            currentammo02--;
            shoot=false;
            ammoindex=2;
            Debug.Log("Tipo 2");
        }
    }
    if (ammoindex==3)
    {
        if(shoot==true)
        {
            currentammo03--;
            shoot=false;
            ammoindex=3;
            Debug.Log("Tipo 3");
        }
    }
    if (ammoindex==4)
    {
        if(shoot==true)
        {
            currentammo04--;
            shoot=false;
            ammoindex=4;
            Debug.Log("Tipo 4");
        }
    }            
}    
}


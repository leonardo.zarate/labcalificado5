using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using UnityEngine.UI;

public class ports : MonoBehaviour
{
    string[] puertos;
    SerialPort arduino;
    public Dropdown lista_puertos;
    string puerto_sleccionado;
    bool conectado=false;
    public Text text;
    public GameObject cube;
    private void Awake()
    {
        lista_puertos.options.Clear();
        puertos= SerialPort.GetPortNames();
        
        foreach(string port in puertos)
        {
            lista_puertos.options.Add(new Dropdown.OptionData(){text= port});
            
        }
        DropdownItemSelected(lista_puertos);
        lista_puertos.onValueChanged.AddListener(delegate{DropdownItemSelected(lista_puertos);});
        
        
    }

    void DropdownItemSelected(Dropdown lista)
    {
        int indice = lista.value;
        puerto_sleccionado= lista_puertos.options[indice].text;
        Debug.Log(puerto_sleccionado);
    }
    public void Conectar()
    {
        if(conectado== false)
        {
            arduino= new SerialPort(puerto_sleccionado,9600,Parity.None,8,StopBits.One);
            arduino.Open();
            Debug.Log("conectado");
            conectado=true;
        }

    } 
    public void Desconectar()
    {
        if(conectado==true)
        {
            arduino.Close();
            Debug.Log("desconectado");
        }

    }
    public void Send()
    {
       if(conectado) 
        {
            arduino.WriteLine(text.text);
            string rest=arduino.ReadLine();
            Debug.Log(rest);
        }
    } 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (conectado)
        {
            string resul = arduino.ReadLine();
            string[] numbers = resul.Split('/');
            int cz = int.Parse(numbers[0]); 
            int cx = int.Parse(numbers[1]);
            int cy = int.Parse(numbers[2]);

            cube.transform.rotation = Quaternion.Euler(cx, cy, cz);


            if(Input.GetKeyDown(KeyCode.Alpha1))
                arduino.Write("1");
            if(Input.GetKeyDown(KeyCode.Alpha2))
                arduino.Write("2");
            if(Input.GetKeyDown(KeyCode.Alpha3))
                arduino.Write("3");
            if(Input.GetKeyDown(KeyCode.Alpha4))
                arduino.Write("4");
            if(Input.GetKeyDown(KeyCode.Mouse0))
                arduino.Write("s");
            if(Input.GetKeyDown(KeyCode.R))        
                arduino.Write("r");            
        }

    }
}

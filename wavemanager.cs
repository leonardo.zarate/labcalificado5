﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class wavemanager : MonoBehaviour
{
    public List<WaveObject> waves= new List<WaveObject>();
    public bool waiting;
    public int index;
    public Transform initpos;
    public bool endwave;

    public TextMeshProUGUI counterText;
    public GameObject buttonNextWave;

    private void checkNextWaveCounter()
    {
        if(waiting && !endwave)
        {
            waves[index].countertonextwave-=1*Time.deltaTime;
            counterText.text= waves[index].countertonextwave.ToString("00");
            if( waves[index].countertonextwave <=0)
            {
                ChangeWave();
            }
        }
    }
    private IEnumerator ProcesWave()
    {
        if(endwave)
        {
            yield break;

        }
        waiting = false;
        waves[index].countertonextwave = waves[index].timefornextwave;
        for (int i=0; i<waves[index]. enemys.Count;i++)
        {
            var EnemyGo = Instantiate(waves[index].enemys[i], initpos.position,initpos.rotation);
            yield return new WaitForSeconds(waves[index].timepercreation);
        }
        waiting=true;

        if(index >= waves.Count-1)
        {
            Debug.Log("Nivel Acabado");
            endwave = true;
        }
    }
    private void CheckCounterAndShowButton()
    {
        if(!endwave)
        {
            buttonNextWave.SetActive(waiting);
            counterText.gameObject.SetActive(waiting);
        }
    }
    public void ChangeWave()
    {
        if(endwave)
            return;
        index++;
        StartCoroutine(ProcesWave());
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ProcesWave());
    }

    // Update is called once per frame
    void Update()
    {
        
        CheckCounterAndShowButton();
        checkNextWaveCounter();
    }
}
 [System.Serializable]
public class WaveObject
{
    public float timepercreation= 1;
    public float timefornextwave= 60;
    public float countertonextwave = 0;
    public List<enemies> enemys = new List<enemies>();
}